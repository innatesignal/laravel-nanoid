<?php

namespace Tests;

use InnateSignal\Database\LaravelNanoidServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            LaravelNanoidServiceProvider::class,
        ];
    }
}

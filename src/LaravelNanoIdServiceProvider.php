<?php

namespace InnateSignal\Database;

use Illuminate\Support\Fluent;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\ColumnDefinition;
use Illuminate\Database\Schema\Grammars\Grammar;
use InnateSignal\Database\Exceptions\UnknownGrammarClass;

class LaravelNanoIdServiceProvider extends ServiceProvider
{
    public function register()
    {
        Grammar::macro('typeNanoId', function (Fluent $column) {
            switch (class_basename(static::class)) {
                case 'MySqlGrammar':
                    return sprintf('varchar(%d)', $column->length ?? 21);

                case 'PostgresGrammar':
                    return sprintf('varchar(%d)', $column->length ?? 21);

                case 'SQLiteGrammar':
                    return sprintf('varchar(%d)', $column->length ?? 21);

                default:
                    throw new UnknownGrammarClass;
            }
        });

        Blueprint::macro('nanoId', function ($column): ColumnDefinition {
            return $this->addColumn('nanoId', $column);
        });
    }
}

<?php

namespace InnateSignal\Database\Casts;

use Hidehalo\Nanoid\Client;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class NanoId implements CastsAttributes
{
    /**
     * Transform the attribute from the underlying model values.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        if (trim($value) === '') {
            return;
        }

        return $value;
    }

    /**
     * Transform the attribute to its underlying model values.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return array
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (trim($value) === '') {
            return;
        }

        $size = 21;
        $nano = new Client();

        return [
            $key => $nano->generateId($size, Client::MODE_DYNAMIC)
        ];
    }
}
